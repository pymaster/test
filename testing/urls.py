from django.contrib import admin
from django.urls import path, include, re_path
from .views import SelectTest, TestInfo, Testing, TestResult

urlpatterns = [
    path('', SelectTest , name='all_tests'),
    path('<test_id>/result/', TestResult , name='test_result'),
    path('<test_id>/', TestInfo , name='test_info'),
    path('<test_id>/<question_id>/', Testing , name='testing'),
]
